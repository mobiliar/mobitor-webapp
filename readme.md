# Mobitor

This repository contains the mobitor-application - a Spring Boot based Build Monitor.

It uses the default plugins and base functionality from mobitor-parent.

The examples and instructions can be found on the [mobitor website](https://mobiliar.bitbucket.io/mobitor/).


## Quick Start

 * Fork this repository
 * This repository uses the "vendor" branch as default
 * switch over to your own branch (usually "master")
 * add you own screen-*.json configuration files and modifications
 * don't push those changes back


## Deployment

The build consists of a module "mobitor-application" which builds the Spring Boot
executable jar. It can be packed into a Docker image and run in Kubernetes or any
Docker Container runtime.

The Mobitor does not contain any database - all data is held in memory.

Requirements:

 * Java 11
 * Maven 3.3.X


## Starting Mobitor locally

Some plugins (specifically the REST plugin) collect a lot of information.

We therefore recommend you create your own spring profile (applicion-<profile>.properties)
to disable plugins or change their configuration when starting the Mobitor locally.


## Starting using the Spring Boot Application:

Execute the maven plugin: `mvn spring-boot:run`


## Starting the Docker Image

Execute the Docker goal: `mvn docker:run`

If you are also using Liima to monitor deployments, the embedded Liima client needs a 
client certificate for authentication which you need to pass into the container.


```
docker run -p 8099:18080 -e SPRING_PROFILES_ACTIVE=dev -e MOBITOR_CERT_OPTS= -v ${PWD}/certs:/certs mobiliar/mobitor
```

Using `mvn docker:stop` and `mvn docker:remove` the running container can be stopped and removed.


## Accessing the Mobitor locally

The Docker Container by default runs on: [localhost:19080](http://localhost:19080/mobitor/) 

The spring boot application by default runs on: [localhost:18080](http://localhost:18080/mobitor/)


## Swagger-UI

The swagger ui resides at `/mobitor/swagger-ui.html`


## Online Documentation:

The Mobitor contains its documentation as well as part of the spring boot application: `/mobitor/doc/index.html`
