package ch.mobi.mobitor.it;

/*-
 * §
 * mobitor-application
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentPipeline;
import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import ch.mobi.mobitor.service.TestEnvConfigProps;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;

import javax.xml.xpath.XPathExpressionException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

public class IndexScreenIT extends AbstractScreenIT {

    @MockBean
    private EnvironmentsConfigurationService environmentsConfigurationService;

    @BeforeEach
    public void setup() {
        when(environmentsConfigurationService.getEnvironmentConfig(any())).thenReturn(new TestEnvConfigProps("testenv"));
        when(environmentsConfigurationService.getPipeline(any())).thenReturn(EnvironmentPipeline.ANY);
    }


    public void printHtml() throws Exception {
        performGet().andDo(print());
    }

    @Test
    public void contextLoads() {
        assertThat(getWebApplicationContext()).isNotNull();
    }

    @Test
    public void renderedScreenShouldContainAMenuItemForEachAvailableScreen() throws Exception {
        //given
        List<Screen> availableScreens = getScreensModel().getAvailableScreens();

        //when
        ResultActions resultActions = performGet();

        //then
        resultActions.andExpect(view().name("index"));
        for (Screen screen : availableScreens) {
            resultActions.andExpect(menuContainsScreen(screen));
        }
    }

    @Test
    public void renderedScreenShouldHaveCorrectTitle() throws Exception {
        //given
        Screen screen = findScreen("0");

        //when
        ResultActions resultActions = performGetScreen(screen.getConfigKey());

        //then
        resultActions.andExpect(screenTitleContains(screen.getLabel()));
    }

    @NotNull
    private ResultMatcher menuContainsScreen(Screen screen) throws XPathExpressionException {
        return xpath("/html/body/div/div/ul/li/a[@href='/screen?key=" + screen.getConfigKey() + "']/text()").string(screen.getLabel());
    }

}
