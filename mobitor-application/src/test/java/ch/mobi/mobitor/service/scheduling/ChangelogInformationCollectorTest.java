package ch.mobi.mobitor.service.scheduling;

/*-
 * §
 * mobitor-application
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.ChangelogConfiguration;
import ch.mobi.mobitor.domain.deployment.Deployment;
import ch.mobi.mobitor.domain.deployment.DeploymentApplication;
import ch.mobi.mobitor.domain.factory.ExtendableScreenFactory;
import ch.mobi.mobitor.domain.screen.DefaultScreen;
import ch.mobi.mobitor.plugin.bitbucket.service.client.BitBucketClient;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketCommitInfoResponse;
import ch.mobi.mobitor.plugin.jira.service.client.JiraClient;
import ch.mobi.mobitor.plugin.jira.service.client.domain.IssueFields;
import ch.mobi.mobitor.plugin.jira.service.client.domain.IssueResponse;
import ch.mobi.mobitor.plugin.liima.LiimaDeploymentsPlugin;
import ch.mobi.mobitor.plugin.liima.config.AppServerConfig;
import ch.mobi.mobitor.plugin.liima.service.ServersConfigurationService;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.service.DeploymentInformationService;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import ch.mobi.mobitor.service.config.ChangelogConfigurationService;
import ch.mobi.mobitor.service.plugins.MobitorPluginsRegistry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.DefaultResourceLoader;

import java.util.*;

import static ch.mobi.mobitor.domain.config.AppServerConfigTestFactory.createMinimalAppServerConfigMap;
import static ch.mobi.mobitor.domain.config.ScreenConfigTestFactory.createMinimalScreenConfig;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class ChangelogInformationCollectorTest {

    private ChangelogConfiguration changelogConfiguration;

    @BeforeEach
    public void setupChangelogConfiguration() {
        changelogConfiguration = new ChangelogConfiguration();
        changelogConfiguration.setBlacklistedProjects(asList("PKEY", "PPKEY"));
    }

    @Test
    public void collectBitBucketInformation() {
        // arrange
        ExtendableScreenConfig screenConfig = createMinimalScreenConfig();
        Map<String, AppServerConfig> appSerNameToConfigMap = createMinimalAppServerConfigMap();

        ServersConfigurationService serversConfigurationService = new ServersConfigurationService(new DefaultResourceLoader());
        serversConfigurationService.setAppServerNameToConfigMap(appSerNameToConfigMap);
        List<MobitorPlugin> plugins = singletonList(new LiimaDeploymentsPlugin(serversConfigurationService));
        MobitorPluginsRegistry pluginRegistry = new MobitorPluginsRegistry(Optional.ofNullable(plugins));
        pluginRegistry.initialize();
        ExtendableScreenFactory sf = new ExtendableScreenFactory(pluginRegistry);
        DefaultScreen screen = sf.initializeEmptyScreen(screenConfig);
        DefaultScreen screenSpy = spy(screen);

        ScreensModel screensModel = mock(ScreensModel.class);
        screensModel.addScreen(screenSpy);

        BitBucketClient bitBucketClient = mock(BitBucketClient.class);
        BitBucketCommitInfoResponse bitBucketCommitInfoResponse = new BitBucketCommitInfoResponse();
        Map<String, Object> properties = new HashMap<>();
        properties.put("jira-key", asList("JIRA-1", "JIRA-2"));
        bitBucketCommitInfoResponse.setProperties(properties);

        List<BitBucketCommitInfoResponse> commits = singletonList(bitBucketCommitInfoResponse);
        when(bitBucketClient.retrieveCommits(anyString(), anyString(), anyString())).thenReturn(commits);

        DeploymentInformationService deploymentInformationService = mock(DeploymentInformationService.class);

        Deployment deployment = new Deployment();
        deployment.setEnvironment("B");
        deployment.setServerName("server_name_1");
        deployment.setState("success");
        deployment.setDeploymentDate(System.currentTimeMillis());
        DeploymentApplication deploymentApplication = new DeploymentApplication();
        deploymentApplication.setVersion("1.1.1");
        deploymentApplication.setApplicationName("application_name_1");
        deployment.addDeploymentApplication(deploymentApplication);
        List<Deployment> deployments = singletonList(deployment);

        when(deploymentInformationService.retrieveDeployments(anyString())).thenReturn(deployments);
        when(deploymentInformationService.filterDeployments(anyList(), anySet())).thenReturn(deployments);

        ChangelogConfigurationService changelogConfigurationService = new ChangelogConfigurationService(new DefaultResourceLoader());
        changelogConfigurationService.initializeChangelogRepositories();
        ChangelogConfigurationService changelogConfigurationServiceSpy = spy(changelogConfigurationService);

        EnvironmentsConfigurationService environmentsConfigurationService = mock(EnvironmentsConfigurationService.class);

        JiraClient jiraClient = mock(JiraClient.class);
        IssueResponse issueResponse = new IssueResponse();
        IssueFields issueFields = new IssueFields();
        issueResponse.setFields(issueFields);


        when(jiraClient.retrieveIssue(anyString())).thenReturn(issueResponse);

        ChangelogInformationCollector changelogCollector = new ChangelogInformationCollector(bitBucketClient, jiraClient, deploymentInformationService, changelogConfigurationServiceSpy, environmentsConfigurationService, changelogConfiguration);

        // act
        changelogCollector.collectChangelogInformation();

        // assert
        verify(changelogConfigurationServiceSpy).getAllChangelogConfigs();
        verify(changelogConfigurationServiceSpy).setChangelogRefresh(any(Date.class));
    }

    @Test
    public void testIssueKeyIsInBlacklistedProject() {
        // arrange
        ChangelogInformationCollector changelogInformationCollector = new ChangelogInformationCollector(null, null, null, null, null, changelogConfiguration);

        // act
        assertThat(changelogInformationCollector.issueIsNotInBlacklistedProject("PKEY-1")).isFalse();
        assertThat(changelogInformationCollector.issueIsNotInBlacklistedProject("PPKEY-1")).isFalse();
        assertThat(changelogInformationCollector.issueIsNotInBlacklistedProject("PPPKEY-1")).isTrue();
    }

}
