package ch.mobi.mobitor;

/*-
 * §
 * mobitor-application
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentConfigProperties;
import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.plugin.bitbucket.BitBucketPluginConfiguration;
import ch.mobi.mobitor.plugin.bitbucket.BitBucketRepositoriesPlugin;
import ch.mobi.mobitor.plugin.bitbucket.config.BitBucketRepositoryConfig;
import ch.mobi.mobitor.plugin.edwh.EdwhDeploymentsPlugin;
import ch.mobi.mobitor.plugin.jira.JiraPlugin;
import ch.mobi.mobitor.plugin.jira.config.JiraFilterConfig;
import ch.mobi.mobitor.plugin.jira.service.JiraPluginAttributeProvider;
import ch.mobi.mobitor.plugin.jira.service.client.JiraConfiguration;
import ch.mobi.mobitor.plugin.kubernetes.KubernetesPlugin;
import ch.mobi.mobitor.plugin.liima.LiimaDeploymentsPlugin;
import ch.mobi.mobitor.plugin.liima.config.AppServerConfig;
import ch.mobi.mobitor.plugin.liima.service.ServersConfigurationService;
import ch.mobi.mobitor.plugin.nexusiq.NexusIqPlugin;
import ch.mobi.mobitor.plugin.nexusiq.NexusIqPluginConfiguration;
import ch.mobi.mobitor.plugin.rest.RestPluginConfiguration;
import ch.mobi.mobitor.plugin.rest.RestResourcePlugin;
import ch.mobi.mobitor.plugin.rest.config.AdditionalRestUri;
import ch.mobi.mobitor.plugin.rest.config.RestServiceConfig;
import ch.mobi.mobitor.plugin.rest.service.configservice.LoadBalancerFilterService;
import ch.mobi.mobitor.plugin.rest.service.configservice.RestServiceByReachableEnvironmentFilter;
import ch.mobi.mobitor.plugin.rest.service.configservice.RestServiceFixUrlByEnvironmentProcessor;
import ch.mobi.mobitor.plugin.sonarqube.SonarQubePlugin;
import ch.mobi.mobitor.plugin.sonarqube.config.SonarProjectConfig;
import ch.mobi.mobitor.plugin.sonarqube.service.SonarQubeAttributeProvider;
import ch.mobi.mobitor.plugin.sonarqube.service.client.SonarQubeConfiguration;
import ch.mobi.mobitor.plugin.swd.SwdPlugin;
import ch.mobi.mobitor.plugin.swd.config.SwdDeploymentConfig;
import ch.mobi.mobitor.plugin.teamcity.TeamCityBuildsPlugin;
import ch.mobi.mobitor.plugin.teamcity.config.TeamCityBuildConfig;
import ch.mobi.mobitor.plugin.zaproxy.ZaproxyPlugin;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import ch.mobi.mobitor.service.TestEnvConfigProps;
import ch.mobi.mobitor.service.config.ExtendableScreenConfigService;
import ch.mobi.mobitor.service.config.MobitorApplicationConfiguration;
import ch.mobi.mobitor.service.plugins.MobitorPluginsRegistry;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.keyvalue.MultiKey;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.mock.env.MockEnvironment;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static ch.mobi.mobitor.plugin.rest.config.RestServiceConfig.ENV_VARIABLE;
import static java.lang.String.format;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ScreenConfigurationTest {

    private ExtendableScreenConfigService screenConfigurationService;
    private ServersConfigurationService serversConfigurationService;
    private EnvironmentsConfigurationService environmentsConfigurationService;
    private List<MobitorPlugin> plugins;

    private Map<String, AppServerConfig> allServerNamesToServerConfigMap = new HashMap<>();

    private <T> List<T> getConfigsOfPlugins(String configPropertyName, ExtendableScreenConfig screenConfig) {
        List<T> configs = screenConfig.getPluginConfigMap().get(configPropertyName);
        if (CollectionUtils.isEmpty(configs)) {
            return new ArrayList<>();
        }
        return configs;
    }

    @BeforeEach
    public void setup() {
        MobitorApplicationConfiguration mobitorApplicationConfiguration = new MobitorApplicationConfiguration(new MockEnvironment());
        mobitorApplicationConfiguration.setNetwork(EnvironmentNetwork.PRODUCTION);
        serversConfigurationService = new ServersConfigurationService(new DefaultResourceLoader());
        RestPluginConfiguration restPluginConf = new RestPluginConfiguration();
        restPluginConf.setNetwork(EnvironmentNetwork.PRODUCTION);
        // TODO until the screen config consistency check is part of the plugin, replace this with your own implementation?
        environmentsConfigurationService = mock(EnvironmentsConfigurationService.class);
        List<EnvironmentConfigProperties> envConfPropList = new ArrayList<>();
        envConfPropList.add(new TestEnvConfigProps("build"));
        envConfPropList.add(new TestEnvConfigProps("ci"));
        envConfPropList.add(new TestEnvConfigProps("development"));
        envConfPropList.add(new TestEnvConfigProps("preprod"));
        envConfPropList.add(new TestEnvConfigProps("prod"));
        when(environmentsConfigurationService.getEnvironments()).thenReturn(envConfPropList);

        LoadBalancerFilterService loadBalancerFilterService = new LoadBalancerFilterService(environmentsConfigurationService);
        RestServiceFixUrlByEnvironmentProcessor restServiceUrlProcessor = new RestServiceFixUrlByEnvironmentProcessor(loadBalancerFilterService);

        BitBucketPluginConfiguration bbConfig = new BitBucketPluginConfiguration();

        NexusIqPluginConfiguration nexusIqConfig = new NexusIqPluginConfiguration();
        JiraPluginAttributeProvider screenAttrProvider = new JiraPluginAttributeProvider(new JiraConfiguration());
        SonarQubeAttributeProvider sonarQubeAttrProvider = new SonarQubeAttributeProvider(new SonarQubeConfiguration());

        RestServiceByReachableEnvironmentFilter restServiceEnvFilter = new RestServiceByReachableEnvironmentFilter(environmentsConfigurationService);

        plugins = List.of(
                new BitBucketRepositoriesPlugin(bbConfig),
                new EdwhDeploymentsPlugin(environmentsConfigurationService),
                new JiraPlugin(screenAttrProvider),
                new KubernetesPlugin(),
                new LiimaDeploymentsPlugin(serversConfigurationService),
                new NexusIqPlugin(nexusIqConfig),
                new RestResourcePlugin(restServiceEnvFilter, restServiceUrlProcessor, environmentsConfigurationService),
                new SonarQubePlugin(sonarQubeAttrProvider),
                new TeamCityBuildsPlugin(),
                new SwdPlugin(environmentsConfigurationService),
                new ZaproxyPlugin()
        );
        MobitorPluginsRegistry pluginsRegistry = new MobitorPluginsRegistry(Optional.ofNullable(plugins));
        pluginsRegistry.initialize();
        screenConfigurationService = new ExtendableScreenConfigService(pluginsRegistry, new ArrayList<>());

        // arrange
        serversConfigurationService.initializeAmwDeployments();
        screenConfigurationService.initialize();

        populateServersMap();
    }

    private void populateServersMap() {
        List<AppServerConfig> applicationServers = this.serversConfigurationService.getApplicationServers();
        for (AppServerConfig applicationServer : applicationServers) {
            allServerNamesToServerConfigMap.put(applicationServer.getAppServerName(), applicationServer);
        }

        for (int i = 0; i < 10; i++) {
            // add servers only used for test-screens:
            AppServerConfig serverConfig = new AppServerConfig();
            serverConfig.setApplicationNames(singletonList(format("application_name_%d", i)));
            serverConfig.setAppServerName(format("server_name_%d", i));
            allServerNamesToServerConfigMap.put(format("server_name_%d", i), serverConfig);
        }
    }

    @Test
    public void verifyAtLeastOneScreenConfigAvailable() {
        // arrange
        assertNotNull(this.screenConfigurationService);

        // act
        List<ExtendableScreenConfig> allScreenConfigs = screenConfigurationService.getAllScreenConfigs();

        // assert
        assertThat(allScreenConfigs).isNotEmpty();
    }

    private void verifyScreenConfigurationIntegrity(ExtendableScreenConfig screenConfig) {
        assertNotNull(screenConfig);
        assertNotNull(screenConfig.getConfigKey());
        assertNotNull(screenConfig.getLabel());

        System.out.println("checking integrity of configuration: " + screenConfig.getConfigKey());

        //
        // environment checks
        //
        List<String> environments = screenConfig.getEnvironments();
        assertThat(environments).isNotEmpty();
        for (String env : environments) {
            assertThat(env).isNotBlank();
        }

        List<String> serverNames = screenConfig.getServerNames();
        assertThat(serverNames).isNotNull();

        plugins.forEach(plugin -> {
            String configPropertyName = plugin.getConfigPropertyName();
            List pluginConfigs = getConfigsOfPlugins(configPropertyName, screenConfig);
            assertThat(pluginConfigs).isNotNull();
        });
    }

    @Test
    public void verifyScreenConfigIntegrity() {
        // arrange
        assertNotNull(this.screenConfigurationService);

        // act
        List<ExtendableScreenConfig> allScreenConfigs = screenConfigurationService.getAllScreenConfigs();
        allScreenConfigs.forEach(this::verifyScreenConfigurationIntegrity);

        // assert
        // no exception
    }

    private void verifyTeamCityBuildsMatchAnEnvironment(ExtendableScreenConfig screenConfig) {
        System.out.println("Screen: " + screenConfig.getConfigKey());
        List<String> environments = screenConfig.getEnvironments();
        List<TeamCityBuildConfig> teamCityBuildConfigs = getConfigsOfPlugins("teamCityBuilds", screenConfig);
        for (TeamCityBuildConfig teamCityBuildConfig : teamCityBuildConfigs) {
            String tcEnv = teamCityBuildConfig.getEnvironment();
            assertThat(environments).contains(tcEnv);
        }
    }

    private void verifyPluginConfigEnvironmentIsKnown(ExtendableScreenConfig screenConfig) {
        plugins.forEach(plugin -> {
            String configPropertyName = plugin.getConfigPropertyName();
            List pluginConfigs = getConfigsOfPlugins(configPropertyName, screenConfig);
            // TODO delegate screen consistency checks to plugin:
            // plugin.verifyScreenConfigurationIsConsist(screenConfig, pluginConfigs);
            // For now just check the list is at least empty and not null:
            assertThat(pluginConfigs).isNotNull();
        });
    }

    @Test
    public void verifyPluginsCanVerifyScreenConfigConsistency() {
        List<ExtendableScreenConfig> allScreenConfigs = this.screenConfigurationService.getAllScreenConfigs();
        allScreenConfigs.forEach(this::verifyPluginConfigEnvironmentIsKnown);
    }

    @Test
    // TODO remove once check is done in the plugin
    public void verifyTeamCityBuildsAssociationWithEnvironment() {
        List<ExtendableScreenConfig> allScreenConfigs = this.screenConfigurationService.getAllScreenConfigs();
        allScreenConfigs.forEach(this::verifyTeamCityBuildsMatchAnEnvironment);
    }

    @Test
    // TODO remove once check is done in the plugin
    public void verifySonarReportsAssociationWithEnvironment() {
        List<ExtendableScreenConfig> allScreenConfigs = this.screenConfigurationService.getAllScreenConfigs();
        allScreenConfigs.forEach(this::verifySonarReportsMatchAnEnvironment);
    }

    private void verifySonarReportsMatchAnEnvironment(ExtendableScreenConfig screenConfig) {
        System.out.println("Screen: " + screenConfig.getConfigKey());
        List<String> environments = screenConfig.getEnvironments();
        List<SonarProjectConfig> sonarProjectConfigs = getConfigsOfPlugins("sonarProjects", screenConfig);
        for (SonarProjectConfig sonarProjectConfig : sonarProjectConfigs) {
            String sonarEnv = sonarProjectConfig.getEnvironment();
            assertThat(environments).contains(sonarEnv);
        }
    }

    // TODO remove once check is done in the plugin
    @Test
    public void verifyAppServersFoundInAmwServerConfig() {
        // arrange
        // act
        // assert
        List<ExtendableScreenConfig> allScreenConfigs = this.screenConfigurationService.getAllScreenConfigs();
        for (ExtendableScreenConfig screenConfig : allScreenConfigs) {
            System.out.println("checking appServers on screen: " + screenConfig.getConfigKey());
            List<String> liimaServers = getConfigsOfPlugins("liimaServers", screenConfig);

            liimaServers.removeAll(allServerNamesToServerConfigMap.keySet());

            assertThat(liimaServers).isEmpty();
        }
    }

    @Test
    // TODO remove once check is done in the plugin
    public void verifyLiimaServersAreInServerNamesList() {
        List<ExtendableScreenConfig> allScreenConfigs = screenConfigurationService.getAllScreenConfigs();
        for (ExtendableScreenConfig screenConfig : allScreenConfigs) {
            System.out.println("Screen Config for key: " + screenConfig.getConfigKey());

            List<String> liimaServers = getConfigsOfPlugins("liimaServers", screenConfig);

            for (String liimaServerName : liimaServers) {
                assertThat(screenConfig.getServerNames()).contains(liimaServerName);
            }
        }
    }

    @Test
    // TODO remove once check is done in the plugin
    public void verifyTeamCityBuildsAssociationServer() {
        List<ExtendableScreenConfig> allScreenConfigs = screenConfigurationService.getAllScreenConfigs();
        for (ExtendableScreenConfig screenConfig : allScreenConfigs) {
            System.out.println("Screen Config for key: " + screenConfig.getConfigKey());

            List<TeamCityBuildConfig> teamCityBuildConfigs = getConfigsOfPlugins("teamCityBuilds", screenConfig);
            for (TeamCityBuildConfig tcConfig : teamCityBuildConfigs) {
                String serverName = tcConfig.getServerName();
                String amwApplicationName = tcConfig.getApplicationName();
                System.out.println("Server: " + serverName  + " Application: " + amwApplicationName);
                assertThat(screenConfig.getServerNames()).contains(serverName);
            }
        }
    }

    @Test
    // TODO remove once check is done in the plugin
    public void verifySonarProjectAssociationAmwServer() {
        List<ExtendableScreenConfig> allScreenConfigs = this.screenConfigurationService.getAllScreenConfigs();
        for (ExtendableScreenConfig screenConfig : allScreenConfigs) {
            System.out.println("Screen Config for key: " + screenConfig.getConfigKey());

            List<SonarProjectConfig> sonarConfigs = getConfigsOfPlugins("sonarProjects", screenConfig);
            for (SonarProjectConfig sonarConfig : sonarConfigs) {
                // a sonar project is either associated with a amw server
                // or with a teamcity build
                String serverName = sonarConfig.getServerName();
                String amwApplicationName = sonarConfig.getApplicationName();

                assertThat(screenConfig.getServerNames()).contains(serverName);
            }
        }
    }

    @Test
    public void verifyScreenEnvironmentsAreMapped() {
        List<EnvironmentConfigProperties> environments = environmentsConfigurationService.getEnvironments();
        List<String> allEnvironments = environments.stream().map(EnvironmentConfigProperties::getEnvironment).collect(Collectors.toList());

        List<ExtendableScreenConfig> allScreenConfigs = this.screenConfigurationService.getAllScreenConfigs();
        for (ExtendableScreenConfig screenConfig : allScreenConfigs) {
            List<String> screenConfigEnvironments = screenConfig.getEnvironments();

            screenConfigEnvironments.removeAll(allEnvironments);

            System.out.println("Screen: " + screenConfig.getConfigKey() + " " + screenConfig.getLabel());
            assertThat(screenConfigEnvironments).isEmpty();
        }
    }

    @Test
    // TODO remove once check is done in the plugin
    public void testGetRestServiceConfigsPerEnvironment() {
        List<ExtendableScreenConfig> allScreenConfigs = this.screenConfigurationService.getAllScreenConfigs();
        for (ExtendableScreenConfig screenConfig : allScreenConfigs) {
            List<RestServiceConfig> restServiceConfigs = getConfigsOfPlugins("restServices", screenConfig);

            assertThat(restServiceConfigs).isNotNull();
        }
    }

    @Test
    // TODO remove once check is done in the plugin
    public void testGetRestServiceConfigsContainsEnvVarOrEnvField() {
        ExtendableScreenConfig screenConfig = this.screenConfigurationService.getScreenConfig("testrestperenv");

        List<RestServiceConfig> restServiceConfigs = getConfigsOfPlugins("restServices", screenConfig);

        assertThat(restServiceConfigs).isNotNull();

        for (RestServiceConfig restServiceConfig : restServiceConfigs) {
            if (restServiceConfig.getSwaggerUri().contains(ENV_VARIABLE)) {
                assertThat(restServiceConfig.getEnvironment()).isNull();
            } else {
                assertThat(restServiceConfig.getEnvironment()).isNotBlank();
            }
        }
    }

    @Test
    // TODO remove once check is done in the plugin
    public void testLoadBalancerUrisStartWithHttps() {
        List<ExtendableScreenConfig> allScreenConfigs = screenConfigurationService.getAllScreenConfigs();
        for (ExtendableScreenConfig screenConfig : allScreenConfigs) {
            List<RestServiceConfig> restServiceConfigs = getConfigsOfPlugins("restServices", screenConfig);
            for (RestServiceConfig restServiceConfig : restServiceConfigs) {

                boolean httpsOrEmpty = isEmpty(restServiceConfig.getSwaggerUri()) || restServiceConfig.getSwaggerUri().startsWith("https://");
                assertThat(httpsOrEmpty).isTrue();

                List<AdditionalRestUri> additionalUris = restServiceConfig.getAdditionalUris();
                if (CollectionUtils.isNotEmpty(additionalUris)) {
                    additionalUris.forEach(addUri -> assertThat(addUri.getUri()).startsWith("https://"));
                }
            }
        }
    }

    @Test
    // TODO remove once check is done in the plugin
    public void testRestServiceRegexPatternsCompile() {
        List<ExtendableScreenConfig> allScreenConfigs = screenConfigurationService.getAllScreenConfigs();
        for (ExtendableScreenConfig screenConfig : allScreenConfigs) {
            List<RestServiceConfig> restServiceConfigs = getConfigsOfPlugins("restServices", screenConfig);
            for (RestServiceConfig restServiceConfig : restServiceConfigs) {
                List<AdditionalRestUri> additionalUris = restServiceConfig.getAdditionalUris();
                additionalUris.stream()
                              .filter(addIru -> isNotEmpty(addIru.getValidationRegex()))
                              .forEach(addUri -> {
                                  String regex = addUri.getValidationRegex();
                                  Pattern pattern = Pattern.compile(regex);
                                  assertThat(pattern).isNotNull();
                });
            }
        }
    }

    @Test
    // TODO remove once check is done in the plugin
    public void testTeamCityConfigIdsNotNull() {
        List<ExtendableScreenConfig> allScreenConfigs = screenConfigurationService.getAllScreenConfigs();
        for (ExtendableScreenConfig screenConfig : allScreenConfigs) {
            List<TeamCityBuildConfig> teamCityBuildConfigs = getConfigsOfPlugins("teamCityBuilds", screenConfig);
            for (TeamCityBuildConfig tcConfig : teamCityBuildConfigs) {
                assertThat(tcConfig.getConfigId()).isNotEmpty();
            }
        }
    }

    @Test
    // TODO remove once check is done in the plugin
    public void testJiraFilterIdNotNull() {
        List<ExtendableScreenConfig> allScreenConfigs = screenConfigurationService.getAllScreenConfigs();
        for (ExtendableScreenConfig screenConfig : allScreenConfigs) {
            List<String> environments = screenConfig.getEnvironments();

            List<JiraFilterConfig> jiraFilterConfigs = getConfigsOfPlugins("jiraFilters", screenConfig);
            for (JiraFilterConfig jiraFilterConfig : jiraFilterConfigs) {
                assertThat(jiraFilterConfig.getFilterId()).isNotEmpty();
                assertThat(jiraFilterConfig.getServerName()).isNotEmpty();
                assertThat(jiraFilterConfig.getApplicationName()).isNotEmpty();
                String environment = jiraFilterConfig.getEnvironment();
                assertThat(environment).isNotEmpty();
                assertThat(environments).contains(environment);
            }
        }
    }

    @Test
    // TODO remove once check is done in the plugin
    public void verifyBitBucketConfigsAssociatedWithEnvironment() {
        List<ExtendableScreenConfig> allScreenConfigs = this.screenConfigurationService.getAllScreenConfigs();
        for (ExtendableScreenConfig sc : allScreenConfigs) {
            List<String> environments = sc.getEnvironments();
            List<String> serverNames = sc.getServerNames();

            List<BitBucketRepositoryConfig> bbConfigs = getConfigsOfPlugins("bitBucketRepositories", sc);
            for (BitBucketRepositoryConfig bbConfig : bbConfigs) {
                assertThat(bbConfig.getServerName()).isNotEmpty();
                assertThat(bbConfig.getApplicationName()).isNotEmpty();
                assertThat(bbConfig.getEnvironment()).isNotEmpty();
                assertThat(bbConfig.getProject()).isNotEmpty();
                assertThat(bbConfig.getRepository()).isNotEmpty();

                assertThat(environments).contains(bbConfig.getEnvironment());
                assertThat(serverNames).contains(bbConfig.getServerName());
            }
        }
    }

    @Test
    // TODO remove once check is done in the plugin
    public void verifyRestConfigUriIsSameForAllServersAppsEnvs() {
        Map<MultiKey<String>, String> allRestServices = new HashMap<>();
        Map<MultiKey<String>, String> keyToScreen = new HashMap<>();

        List<ExtendableScreenConfig> allScreenConfigs = this.screenConfigurationService.getAllScreenConfigs();
        for (ExtendableScreenConfig screenConfig : allScreenConfigs) {
            System.out.println("Screen Config for key: " + screenConfig.getConfigKey());

            List<RestServiceConfig> restServices = getConfigsOfPlugins("restServices", screenConfig);
            for (RestServiceConfig restService : restServices) {
                String currentScreenKey = screenConfig.getConfigKey();

                String serverName = restService.getServerName();
                String applicationName = restService.getApplicationName();
                String env = restService.getEnvironment();

                String uri = restService.getSwaggerUri() == null ? "empty" : restService.getSwaggerUri();

                MultiKey<String> key = new MultiKey<>(serverName, applicationName, env);

                if (allRestServices.containsKey(key)) {
                    // uri of the current rest service must be the same as the one from the map:
                    String prevUri = allRestServices.get(key);
                    String originScreen = keyToScreen.get(key);

                    // only compare the uri's if they are on different screens:
                    if (!originScreen.equals(currentScreenKey)) {
                        assertThat(uri).isEqualTo(prevUri);
                    }

                } else {
                    allRestServices.put(key, uri);
                    keyToScreen.put(key, currentScreenKey);
                }
            }
        }
    }

    @Test
    // TODO remove once check is done in the plugin
    public void verifyRestConfigAssociationServer() {
        List<ExtendableScreenConfig> allScreenConfigs = this.screenConfigurationService.getAllScreenConfigs();
        for (ExtendableScreenConfig screenConfig : allScreenConfigs) {
            System.out.println("Screen Config for key: " + screenConfig.getConfigKey());

            List<RestServiceConfig> restServices = getConfigsOfPlugins("restServices", screenConfig);
            for (RestServiceConfig restService : restServices) {
                String serverName = restService.getServerName();
                String applicationName = restService.getApplicationName();

                assertThat(serverName).isNotEmpty();
                assertThat(applicationName).isNotEmpty();

                AppServerConfig matchingServer = allServerNamesToServerConfigMap.get(serverName);

                assertThat(matchingServer).isNotNull();
                assertThat(matchingServer.getApplicationNames()).contains(applicationName);

                assertThat(screenConfig.getServerNames()).contains(serverName);
            }
        }
    }

    @Test
    // TODO remove once check is done in the plugin
    public void verifyRestConfigAdditionalUris() {
        // if swaggerUri is empty, an additional uri must exist

        List<ExtendableScreenConfig> allScreenConfigs = this.screenConfigurationService.getAllScreenConfigs();
        for (ExtendableScreenConfig screenConfig : allScreenConfigs) {
            System.out.println("Screen Config for key: " + screenConfig.getConfigKey());

            List<RestServiceConfig> restServices = getConfigsOfPlugins("restServices", screenConfig);
            for (RestServiceConfig restService : restServices) {
                String swaggerUri = restService.getSwaggerUri();
                if (isEmpty(swaggerUri)) {
                    // if no swagger-json uri is given, additional uris must be specified
                    assertThat(restService.getAdditionalUris()).isNotEmpty();
                }

                // additionalUris must contain an uri (empty validationRegex - Status Code check)
                restService.getAdditionalUris().forEach(addUri -> assertThat(addUri.getUri()).isNotEmpty());
            }
        }
    }

    @Test
    // TODO remove once check is done in the plugin
    public void verifyIfEnvVariableUsedEnvironmentMustBeEmpty() {
        List<ExtendableScreenConfig> allScreenConfigs = this.screenConfigurationService.getAllScreenConfigs();
        for (ExtendableScreenConfig screenConfig : allScreenConfigs) {
            System.out.println("Screen Config for key: " + screenConfig.getConfigKey());

            List<RestServiceConfig> restServices = getConfigsOfPlugins("restServices", screenConfig);
            for (RestServiceConfig restService : restServices) {
                String swaggerUri = restService.getSwaggerUri();
                if (isNotEmpty(swaggerUri) && swaggerUri.contains(ENV_VARIABLE)) {
                    // if ${env} is used in swagger uri, 'environment:' must not be set
                    assertThat(restService.getEnvironment()).isNull();
                }

                List<AdditionalRestUri> additionalUris = restService.getAdditionalUris();
                additionalUris.forEach(addUri -> {
                    if (addUri.getUri().contains(ENV_VARIABLE)) {
                        // if ${env} is used in additional uri, 'environment:' must not be set
                        assertThat(restService.getEnvironment()).isNull();
                    }
                });
            }
        }
    }

    @Test
    // TODO remove once check is done in the plugin
    public void verifySwdDeploymentsAssociationServerNames() {
        List<ExtendableScreenConfig> allScreenConfigs = this.screenConfigurationService.getAllScreenConfigs();
        allScreenConfigs.forEach(this::verifySwdDeploymentMatchAServerName);
    }

    private void verifySwdDeploymentMatchAServerName(ExtendableScreenConfig screenConfig) {
        System.out.println("Screen: " + screenConfig.getConfigKey());
        List<SwdDeploymentConfig> swdConfigs = getConfigsOfPlugins("swdDeployments", screenConfig);
        for (SwdDeploymentConfig swdConfig : swdConfigs) {
            assertThat(screenConfig.getServerNames()).contains(swdConfig.getServerName());
        }
    }

}
