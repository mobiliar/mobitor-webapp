package ch.mobi.mobitor.domain.configgenerator.generator;

/*-
 * §
 * mobitor-application
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.configgenerator.ScreenConfigGenerator;
import ch.mobi.mobitor.plugin.liima.service.ServersConfigurationService;
import ch.mobi.mobitor.plugin.rest.config.AdditionalRestUri;
import ch.mobi.mobitor.plugin.rest.config.RestServiceConfig;
import ch.mobi.mobitor.plugin.sonarqube.config.SonarProjectConfig;
import ch.mobi.mobitor.plugin.swd.config.SwdDeploymentConfig;
import ch.mobi.mobitor.plugin.teamcity.config.TeamCityBuildConfig;
import ch.mobi.mobitor.plugin.teamcity.config.TeamCityProjectConfig;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static ch.mobi.mobitor.domain.configgenerator.generator.ScreenConfigGeneratorHelper.addAllRestServiceConfigs;
import static ch.mobi.mobitor.domain.configgenerator.generator.ScreenConfigGeneratorHelper.addBuildConfigsWithServerAssociation;
import static ch.mobi.mobitor.domain.configgenerator.generator.ScreenConfigGeneratorHelper.addProjectConfigsWithServerAssociation;
import static ch.mobi.mobitor.domain.configgenerator.generator.ScreenConfigGeneratorHelper.addSonarConfigsWithServerAssociation;
import static ch.mobi.mobitor.plugin.rest.config.RestServiceConfig.ENV_VARIABLE;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public abstract class AbstractScreenConfigGenerator implements ScreenConfigGenerator {

    protected final ServersConfigurationService serversConfigurationService;

    protected AbstractScreenConfigGenerator(ServersConfigurationService serversConfigurationService) {
        this.serversConfigurationService = serversConfigurationService;
    }

    @Override
    public ExtendableScreenConfig generateExtendableScreenConfig(List<ExtendableScreenConfig> screenConfigs) {
        return initializeExtendableScreen(screenConfigs, getConfigKey(), getLabel(), getEnvironments());
    }

    protected abstract String getConfigKey();

    protected abstract String getLabel();

    protected abstract Predicate<String> serverFilterPredicate();

    public abstract List<String> getEnvironments();

    private ExtendableScreenConfig initializeExtendableScreen(List<ExtendableScreenConfig> screenConfigs, String configKey, String label, List<String> environments) {
        ExtendableScreenConfig screen = new ExtendableScreenConfig();
        screen.setConfigKey(configKey);
        screen.setLabel(label);
        screen.setRefreshInterval(-1);
        screen.setEnvironments(getEnvironments());

        // manual interest filters:
        LinkedHashSet<String> servers = new LinkedHashSet<>();
        LinkedHashMap<String, TeamCityBuildConfig> builds = new LinkedHashMap<>();
        LinkedHashMap<String, TeamCityProjectConfig> projects = new LinkedHashMap<>();
        LinkedHashMap<String, SonarProjectConfig> sonars = new LinkedHashMap<>();
        LinkedHashMap<String, RestServiceConfig> restServices = new LinkedHashMap<>();
        Set<SwdDeploymentConfig> swdDeploymentConfigs = new HashSet<>();

        for (ExtendableScreenConfig screenConfig : screenConfigs) {
            List<String> liimaServers = screenConfig.getPluginConfigMap().get("liimaServers");
            List<String> matchingServers = liimaServers.stream()
                                                       .filter(serverFilterPredicate())
                                                       .collect(Collectors.toList());
            servers.addAll(matchingServers);

            // TODO iterate over plugins, don't hardcode these references anymore:
            List<TeamCityBuildConfig> tcBuildConfigs = screenConfig.getPluginConfigMap().get("teamCityBuilds");
            if (CollectionUtils.isNotEmpty(tcBuildConfigs)) {
                List<TeamCityBuildConfig> matchingTcConfigs = filterMatchingTeamCityBuildConfigurations(tcBuildConfigs, environments, matchingServers);
                addBuildConfigsWithServerAssociation(matchingTcConfigs, builds);
            }

            List<TeamCityProjectConfig> teamCityProjectConfigs = screenConfig.getPluginConfigMap().get("teamCityProjects");
            if (CollectionUtils.isNotEmpty(teamCityProjectConfigs)) {
                List<TeamCityProjectConfig> matchingTcProjects = filterMatchingTeamCityProjectsConfigurations(teamCityProjectConfigs, environments, matchingServers);
                addProjectConfigsWithServerAssociation(matchingTcProjects, projects);
            }

            List<SonarProjectConfig> sonarProjectConfigs = screenConfig.getPluginConfigMap().get("sonarProjects");
            if (CollectionUtils.isNotEmpty(sonarProjectConfigs)) {
                List<SonarProjectConfig> matchingSonarConfigs = filterSonarProjectConfigurations(sonarProjectConfigs, environments, matchingServers);
                addSonarConfigsWithServerAssociation(matchingSonarConfigs, sonars);
            }

            List<RestServiceConfig> restServiceConfigsUnfiltered = screenConfig.getPluginConfigMap().get("restServices");
            if (CollectionUtils.isNotEmpty(restServiceConfigsUnfiltered)) {
                List<RestServiceConfig> matchingRestConfigs = filterRestServiceConfigs(restServiceConfigsUnfiltered, matchingServers);
                addAllRestServiceConfigs(matchingRestConfigs, restServices);
            }

            List<SwdDeploymentConfig> swdConfigs = screenConfig.getPluginConfigMap().get("swdDeployments");
            if (CollectionUtils.isNotEmpty(swdConfigs)) {
                List<SwdDeploymentConfig> matchingSwdDeploymentConfigs = filterSwdDeploymentConfigurations(swdConfigs, environments, matchingServers);
                swdDeploymentConfigs.addAll(matchingSwdDeploymentConfigs);
            }
        }

        List<String> appServerConfigs = new ArrayList<>(servers);
        appServerConfigs.sort(String::compareTo);

        screen.setServerNames(appServerConfigs);
        screen.getPluginConfigMap().put("liimaServers", appServerConfigs);

//        screen.setTeamCityBuilds(teamCityBuildConfigs);
        screen.getPluginConfigMap().put("teamCityBuilds", new ArrayList<>(builds.values()));
//        screen.setTeamCityProjects(teamCityProjectConfigs);
        screen.getPluginConfigMap().put("teamCityProjects", new ArrayList<>(projects.values()));
//        screen.setSonarProjects(sonarProjectConfigs);
        screen.getPluginConfigMap().put("sonarProjects", new ArrayList<>(sonars.values()));
//        screen.setRestServices(new ArrayList<>(restServices.values()));
        screen.getPluginConfigMap().put("restServices", new ArrayList<>(restServices.values()));
        // don't add nexus iq to the overall screen (for now), add empty List:
//        screen.setNexusIqApplications(new ArrayList<>());
//        screen.setSwdDeployments(new ArrayList<>(swdDeploymentConfigs));
        screen.getPluginConfigMap().put("swdDeployments", new ArrayList<>(swdDeploymentConfigs));

        return screen;
    }

    private List<TeamCityBuildConfig> filterMatchingTeamCityBuildConfigurations(List<TeamCityBuildConfig> configurations, List<String> environments, List<String> matchingServers) {
        return configurations.stream()
                             .filter(config -> environments.contains(config.getEnvironment()))
                             .filter(config -> matchingServers.contains(config.getServerName()))
                             .collect(Collectors.toList());
    }

    private List<TeamCityProjectConfig> filterMatchingTeamCityProjectsConfigurations(List<TeamCityProjectConfig> configurations, List<String> environments, List<String> matchingServers) {
        return configurations.stream()
                             .filter(config -> environments.contains(config.getEnvironment()))
                             .filter(config -> matchingServers.contains(config.getServerName()))
                             .collect(Collectors.toList());
    }

    private List<SonarProjectConfig> filterSonarProjectConfigurations(List<SonarProjectConfig> configurations, List<String> environments, List<String> matchingServers) {
        return configurations.stream()
                             .filter(config -> environments.contains(config.getEnvironment()))
                             .filter(config -> matchingServers.contains(config.getServerName()))
                             .collect(Collectors.toList());
    }

    private List<SwdDeploymentConfig> filterSwdDeploymentConfigurations(List<SwdDeploymentConfig> configurations, List<String> environments, List<String> matchingServers) {
        return configurations.stream()
                             .filter(config -> matchingServers.contains(config.getServerName()))
                             .collect(Collectors.toList());
    }

    // get rest configs for our list of environments (may contain some that are not mentioned elsewhere)
    private List<RestServiceConfig> filterRestServiceConfigs(List<RestServiceConfig> restServiceConfigs, List<String> matchingServers) {
        List<RestServiceConfig> matchingRestConfigs = restServiceConfigs
                .stream()
                .filter(config -> matchingServers.contains(config.getServerName()))
                .filter(config ->
                        getEnvironments().contains(config.getEnvironment()) ||
                        (isNotEmpty(config.getSwaggerUri()) && config.getSwaggerUri().contains(ENV_VARIABLE)) ||
                        (CollectionUtils.isNotEmpty(config.getAdditionalUris()) && anyAdditionalUriContainsEnvVariable(config.getAdditionalUris())))
                .collect(Collectors.toList());

        return matchingRestConfigs;
    }

    private boolean anyAdditionalUriContainsEnvVariable(List<AdditionalRestUri> uris) {
        boolean result = false;
        for (AdditionalRestUri addUri : uris) {
            result = result || addUri.getUri().contains(ENV_VARIABLE);
        }

        return result;
    }

}
