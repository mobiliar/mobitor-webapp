package ch.mobi.mobitor.domain.configgenerator.generator;

/*-
 * §
 * mobitor-application
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.liima.service.ServersConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

@Component
public class OverallScreenConfigGenerator extends AbstractScreenConfigGenerator {

    @Autowired
    public OverallScreenConfigGenerator(ServersConfigurationService serversConfigurationService) {
        super(serversConfigurationService);
    }

    @Override
    protected String getConfigKey() {
        return "O";
    }

    @Override
    protected String getLabel() {
        return "Übersicht - ALLE Komponenten";
    }

    @Override
    protected Predicate<String> serverFilterPredicate() {
        return serverName -> true;
    }

    @Override
    public List<String> getEnvironments() {
        return Arrays.asList("W", "V", "Z", "I", "B", "T", "P");
    }
}
